﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	public int value;
	private Vector3 destination;
	
	// Use this for initialization
	void Start () {
		destination = Camera.main.ViewportToWorldPoint(new Vector3(0,0.92f,15));
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(transform.position, destination) > 1)
		{
			transform.position = Vector3.MoveTowards(transform.position, destination, 0.5f);
		}
		else
		{
			AudioManager.instance.PlayOnce(3,0.3f);
			GameManager.instance.AddMoney(value);
			Destroy(gameObject);
		}
	}
}
