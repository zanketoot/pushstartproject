﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	
	//mensagem
	public Text messageText;
	public Button positiveMsg;
	public Button negativeMsg;
	public Animator messageAnim;
	//	
	public Texture buildingArea;
	public GameObject[] buildings;
	public Button[] buildingsButton;
	public Text nicknameText;
	public Text moneyText;

	public float camSpeed = 0.05f;
	static public GameManager instance = null;

	public class PlayerStatus
	{
		public int money;
		public string nickname;
	}

	private Transform camTransform;

	private List<GameObject> activeBuildings = new List<GameObject>();
	private GameObject selectedBuilding;
	private int buildingToBuild;
	private int currentBuildingCost;
	private bool building = false;

	private bool holdCam = false;
	static public PlayerStatus ps = new PlayerStatus();

	private bool paused = false;

	private Vector2 textureSize;

	void Awake()
	{
		if(instance == null)
			instance = this;
		else
			Destroy(gameObject);
	}

	// Use this for initialization
	void Start () {
		textureSize = new Vector2(Screen.width/7.5f,Screen.height/5f);
		camTransform = Camera.main.transform;
		AudioManager.instance.PlayBGM();
		GetCash();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && !holdCam) {
             Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
             camTransform.Translate(-touchDeltaPosition.x * camSpeed, -touchDeltaPosition.y * camSpeed, 0);
        }

		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			holdCam = false;
		}
	}

	void OnGUI()
	{
		if(holdCam && Input.touchCount > 0 && !paused)
		{
			GUI.DrawTexture(new Rect(Input.mousePosition.x - (textureSize.x/2), (Input.mousePosition.y + (textureSize.y/2) - Screen.height) * -1, textureSize.x, textureSize.y),buildingArea);
			//GUI.DrawTexture(new Rect(Input.GetTouch(0).position.x - 40f, (Input.GetTouch(0).position.y + 40f - Screen.height) * -1, 80, 80),buildingArea);
		}
			//GUI.DrawTexture(new Rect(Input.mousePosition.x - 40f, (Input.mousePosition.y + 40f - Screen.height) * -1, 80, 80),buildingArea);
	}

	public void AddMoney(int value)
	{
		ps.money += value;
		moneyText.text = ps.money.ToString();

		CheckBlock();
	}

	public void SpendMoney(int value)
	{
		ps.money -= value;
		moneyText.text = ps.money.ToString();

		CheckBlock();
	}

	public void GetCash()
	{
		StartCoroutine("CoGetCash");
	}

	IEnumerator CoGetCash()
	{
		UnityWebRequest www = UnityWebRequest.Get("http://dev.pushstart.com.br/desafio/public/api/status");
		www.SetRequestHeader("X-Authorization", PlayerPrefs.GetString("token"));
        yield return www.Send();

		JsonUtility.FromJsonOverwrite(www.downloadHandler.text, ps);

		nicknameText.text = ps.nickname;
		moneyText.text = ps.money.ToString();
	}

	public void ConstructBuilding(int buildingIndex)
	{
		if(!building)
		{
			Vector3 mouseWorldPosition = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 15f);
			//Vector3 mouseWorldPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 15f);
			mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseWorldPosition);
			
			Collider2D buildingCast = Physics2D.OverlapBox(mouseWorldPosition,new Vector2(3,3),0f);
			
			if(ps.money >= currentBuildingCost && !paused)
			{
				if(buildingCast != null)
				{
					buildingToBuild = buildingIndex;
					selectedBuilding = buildingCast.gameObject;
					ShouldDestroyBuilding(buildingCast.gameObject);
					building = true;
				}
				else
				{
					GameObject tempBuilding = Instantiate(buildings[buildingIndex],mouseWorldPosition,Quaternion.identity);
					activeBuildings.Add(tempBuilding);
					SpendMoney(currentBuildingCost);
				}
			}
		}
		
	}

	void ShouldDestroyBuilding(GameObject building)
	{
		messageText.text = "Do you want to destroy the current building and build a new one on its place?";
		
		positiveMsg.onClick.RemoveAllListeners();
		negativeMsg.onClick.RemoveAllListeners();

		positiveMsg.onClick.AddListener(DestroyAndBuild);
		negativeMsg.onClick.AddListener(HideMsg);

		messageAnim.SetBool("show",true);
	}

	void DestroyAndBuild()
	{
		GameObject tempBuilding = Instantiate(buildings[buildingToBuild],selectedBuilding.transform.position,Quaternion.identity);
		activeBuildings.Add(tempBuilding);
		SpendMoney(currentBuildingCost);
		activeBuildings.Remove(selectedBuilding);
		Destroy(selectedBuilding);
		messageAnim.SetBool("show",false);
		building = false;
	}

	void HideMsg()
	{
		messageAnim.SetBool("show",false);
		building = false;
	}

	public void SetBuildingCost(int cost)
	{
		currentBuildingCost = cost;
		holdCam = true;
	}

	void CheckBlock()
	{
		int blockIndex = -1;

		if(ps.money < 10)
		{
			blockIndex = 4;
		}
		else if(ps.money < 20)
		{
			blockIndex = 3;
		}
		else if(ps.money < 30)
		{
			blockIndex = 2;
		}
		else if(ps.money < 40)
		{
			blockIndex = 1;
		}
		else if(ps.money < 50)
		{
			blockIndex = 0;
		}

		for(int i = 0; i < buildingsButton.Length; i++)
		{
			if(i <= blockIndex)
				buildingsButton[i].interactable = false;
			else
				buildingsButton[i].interactable = true;
		}
	}

	public void PauseGame()
	{
		paused = !paused;

		if(paused)
		{
			AudioManager.instance.PauseBGM();
			foreach(GameObject building in activeBuildings)
			{
				building.SendMessage("PauseConstruction");
			}

			for(int i = 0; i < buildingsButton.Length; i++)
			{
				buildingsButton[i].interactable = false;
			}
		}
		else
		{
			AudioManager.instance.ResumeBGM();
			
			foreach(GameObject building in activeBuildings)
			{
				building.SendMessage("ResumeConstruction");
			}

			CheckBlock();
		}
	}
}
