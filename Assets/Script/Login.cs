﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Security.Cryptography;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

	[System.Serializable]
	
	public class ProfileInfo{
		public string name;
		public string type;
	}

	[System.Serializable]
	public class LoginInfo{
		public ProfileInfo profile;
		public string token;
		public int expires;
	}

	public InputField login;
	public InputField password;

	public Text feedbackText;

	public LoginInfo myLoginInfo = new LoginInfo();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void fLogin()
	{
		feedbackText.text = "Entrando...";
		StartCoroutine("CoLogin");
	}

	IEnumerator CoLogin()
	{
		WWWForm authForm = new WWWForm();
		authForm.AddField("username", login.text.Trim());
		authForm.AddField("password", SHA256Hash(password.text.Trim()));
		

		UnityWebRequest www = UnityWebRequest.Post("http://dev.pushstart.com.br/desafio/public/api/auth/login", authForm);
        yield return www.Send();

        if (www.isNetworkError || www.isHttpError)
        {
			if(www.responseCode == 401)
            	feedbackText.text = "Login ou senha incorretos, tente novamente.";
			else if(www.responseCode == 400)
				feedbackText.text = "Usuário não encontrado, verifique o login digitado.";
			else
				feedbackText.text = "Erro ao tentar logar. Verifique as informações digitadas.";
        }
        else
        {
			JsonUtility.FromJsonOverwrite(www.downloadHandler.text,myLoginInfo);
			PlayerPrefs.SetString("username",myLoginInfo.profile.name);
			PlayerPrefs.SetString("token",myLoginInfo.token);
			PlayerPrefs.SetInt("expires",myLoginInfo.expires);

			SceneManager.LoadScene(1);
        }	
	}

	string SHA256Hash(string text)
 	{
		SHA256 sha256 = new SHA256CryptoServiceProvider();    
		sha256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
		byte[] result = sha256.Hash;        
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < result.Length; i++)
		{
			strBuilder.Append(result[i].ToString("x2"));
		}        
		return strBuilder.ToString();
 	}
}
