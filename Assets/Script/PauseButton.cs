﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour {

	public Sprite pauseImage;
	public Sprite playImage;

	private Image PlayPauseImage; 
	private bool paused = false;

	// Use this for initialization
	void Start () {
		PlayPauseImage = transform.GetChild(0).GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayPause()
	{
		if(paused)
		{
			PlayPauseImage.sprite = pauseImage;
		}
		else
		{
			PlayPauseImage.sprite = playImage;
		}
		paused = !paused;
	}
}
