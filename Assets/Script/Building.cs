﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Building : MonoBehaviour {

	public Sprite buildingSprite;
	public int secondsToBuild;
	public int secondsToProfit;

	public int coinsDrop;

	public Image ConstructionFillBar;

	public GameObject coinPrefab;
	public ParticleSystem pSystem;

	private SpriteRenderer spriteRenderer;
	private GameObject canvasBuilding;

	private float timeLeft = -1f;
	private bool paused = false;
	private bool constructed = false;

	private float lastTimeProfit;
	private float timePaused;
	// Use this for initialization
	void Start () {
		AudioManager.instance.PlayOnce(0);
		canvasBuilding = transform.GetChild(0).gameObject;
		spriteRenderer = GetComponent<SpriteRenderer>();
		StartCoroutine("Construct");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Construct()
	{
		float startTime = Time.time;
		float timeToConstruct = startTime + secondsToBuild;

		if(timeLeft != -1f)
		{
			timeToConstruct = startTime + timeLeft;
		}

		while(Time.time < timeToConstruct && !paused)
		{
			ConstructionFillBar.fillAmount = (timeToConstruct - Time.time) / secondsToBuild;
			yield return new WaitForEndOfFrame();
		}

		if(!paused)
		{
			AudioManager.instance.PlayOnce(1);
			InvokeRepeating("Profit",secondsToProfit,secondsToProfit);
			pSystem.Play();
			spriteRenderer.sprite = buildingSprite;
			canvasBuilding.SetActive(false);
			timeLeft = -1f;
			constructed = true;
		}
		else
		{
			timeLeft = timeToConstruct - Time.time;
		}
	}

	void Profit()
	{
		if(!paused)
		{
			AudioManager.instance.PlayOnce(2,0.5f);
			lastTimeProfit = Time.time;
			for(int i = 0; i < coinsDrop; i++)
			{
				Vector3 randomPos = Random.insideUnitCircle;
				Instantiate(coinPrefab, transform.position + randomPos, Quaternion.identity);
			}
		}
	}

	void PauseConstruction()
	{
		CancelInvoke();
		timePaused = Time.time;
		paused = true;
	}

	void ResumeConstruction()
	{
		InvokeRepeating("Profit", secondsToProfit - (timePaused - lastTimeProfit), secondsToProfit);
		paused = false;
		
		if(!constructed)
		{
			StartCoroutine("Construct");
		}
	}
}
